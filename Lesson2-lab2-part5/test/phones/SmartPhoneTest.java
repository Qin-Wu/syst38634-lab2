package phones;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmartPhoneTest {

	@Test
	public void testGetFormattedPriceRegular() {
		SmartPhone sp=new SmartPhone("iPhone",2000,1.00);
		System.out.println(sp.getFormattedPrice());
		assertTrue("invalid format", "$2,000".equals(sp.getFormattedPrice()));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryIn() {
		SmartPhone sp=new SmartPhone("iPhone",999.0,1.00);
		assertTrue("invalid format", "$999".equals(sp.getFormattedPrice()));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		SmartPhone sp=new SmartPhone("iPhone",999.10,1.00);
		assertFalse("invalid format", "$999.10".equals(sp.getFormattedPrice()));
	}
	
	@Test 
	public void testGetFormattedPriceException() {
		SmartPhone sp=new SmartPhone("iPhone",0.0,1.00);
		assertFalse("invalid format", "$0.0".equals(sp.getFormattedPrice()));
	}
		
	@Test
	public void testSetVersionRegular() throws Exception {
		SmartPhone sp=new SmartPhone();
		sp.setVersion(1.00);	
		double ver=sp.getVersion();
		assertTrue("invalid version", ver==1.00);
	}
	
	@Test
	public void testSetVersionBoundaryIn() throws Exception {
		SmartPhone sp=new SmartPhone();
		sp.setVersion(4.00);	
		double ver=sp.getVersion();
		assertTrue("invalid version", ver==4.00);
	}

	@Test (expected = VersionNumberException.class)
	public void testSetVersionBoundaryOut() throws Exception {
		SmartPhone sp=new SmartPhone();
		sp.setVersion(4.01);	
		double ver=sp.getVersion();
		assertFalse("invalid version", ver==4.01);
	}
	
	@Test (expected = VersionNumberException.class)
	public void testSetVersionException() throws Exception {
		SmartPhone sp=new SmartPhone();
		sp.setVersion(11.0);	
		fail("The version provided is not support");
	}
}
